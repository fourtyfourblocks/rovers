/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.controller;

import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.model.Rover;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : RoverNavigator
 * <p/>
 * description :
 */
public interface RoverNavigator
{
    boolean canMove(Rover rover, Move move);

    Rover move(Rover rover, Move move);

    Rover rotate(Rover rover, Move move);
}
