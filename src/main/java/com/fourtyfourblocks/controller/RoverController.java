/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.controller;

import com.fourtyfourblocks.model.Command;
import com.fourtyfourblocks.model.Heading;
import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.model.Rover;
import com.fourtyfourblocks.rx.Handler;
import com.fourtyfourblocks.rx.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : RoverController
 * <p/>
 * description :
 */
public class RoverController implements RoverNavigator
{
    private final static Logger logger = LoggerFactory.getLogger(RoverController.class);

    private final int maxX;
    private final int maxY;
    private final List<Rover> rovers;

    public RoverController(int maxX, int maxY)
    {
        this.maxX = maxX;
        this.maxY = maxY;
        this.rovers = new ArrayList<>();
    }

    /**
     * navigates rovers around plateau. Makes sure the rover cannot fly into space.
     *
     * @param commands stream
     * @return list of last known rover position.
     */
    public Iterable<Rover> navigate(Observable<Command> commands)
    {
        rovers.clear();
        commands.subscribe(new CommandHandler());
        return rovers;
    }

    /**
     * test current rover position against plateau dimensions.
     *
     * @param rover to be tested
     * @param move  only M allowed
     * @return test result
     */
    @Override
    public boolean canMove(Rover rover, Move move)
    {
        if (move == Move.M)
        {
            final int newX = rover.x + rover.heading.getX();
            final int newY = rover.y + rover.heading.getY();
            return newX >= 0 && newX <= maxX &&
                   newY >= 0 && newY <= maxY;
        }

        return false;
    }

    /**
     * moves rover if it can be moved
     *
     * @param rover to be moved
     * @param move  to be made
     * @return rover
     */
    @Override
    public Rover move(Rover rover, Move move)
    {
        if (canMove(rover, move))
        {
            rover.x += rover.heading.getX();
            rover.y += rover.heading.getY();
        }
        else
        {
            logger.warn("Skipping move {} for {}.", move, rover);
        }

        return rover;
    }

    /**
     * rotates rover
     *
     * @param rover to be rotated
     * @param move  to be made
     * @return rover
     */
    @Override
    public Rover rotate(Rover rover, Move move)
    {
        switch (move)
        {
            case L:
                rover.heading = Heading.previous(rover.heading);
                break;
            case R:
                rover.heading = Heading.next(rover.heading);
                break;
        }

        return rover;
    }

    public int getMaxX()
    {
        return maxX;
    }

    public int getMaxY()
    {
        return maxY;
    }

    private class CommandHandler implements Handler<Command>
    {
        @Override
        public void next(Command event)
        {
            logger.info("Received commands for next rover.");
            final Rover rover = new Rover(event.getX(), event.getY(), event.getHeading());
            rovers.add(rover);

            event.getCommands().subscribe(new MoveHandler(rover, RoverController.this));
        }

        @Override
        public void error(Exception e)
        {
            logger.error("Received error while listening for commands.", e);
        }

        @Override
        public void complete()
        {
            logger.info("All commands has been received.");
        }
    }

}
    
