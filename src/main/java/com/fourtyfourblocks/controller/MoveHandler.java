/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.controller;

import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.model.Rover;
import com.fourtyfourblocks.rx.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : MoveHandler
 * <p/>
 * description :
 */
class MoveHandler implements Handler<Move>
{
    private final static Logger logger = LoggerFactory.getLogger(MoveHandler.class);

    private final Rover rover;
    private final RoverNavigator navigator;

    public MoveHandler(Rover rover, RoverNavigator navigator)
    {
        this.rover = rover;
        this.navigator = navigator;
    }

    @Override
    public void next(Move move)
    {
        switch (move)
        {
            case M:
                navigator.move(rover, move);
                break;
            case L:
                navigator.rotate(rover, move);
                break;
            case R:
                navigator.rotate(rover, move);
                break;
            default:
                logger.warn("Skipping unrecognized move {}.", move);
        }
    }

    @Override
    public void error(Exception e)
    {
        logger.error("Received error while listening for moves.", e);
    }

    @Override
    public void complete()
    {
        logger.info("All moves has been processed.");
    }
}
    
