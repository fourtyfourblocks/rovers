/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.model;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : Heading
 * <p/>
 * description :
 */
public enum Heading
{
    N(0, 1), E(1, 0), S(0, -1), W(-1, 0);

    private final int x;
    private final int y;

    private Heading(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public static Heading previous(Heading current)
    {
        Heading result = current;
        final Heading[] headings = values();
        final int length = headings.length;
        for (int i = 0; i < length; i++)
        {
            Heading heading = headings[i];
            if (heading == current)
            {
                if (i == 0)
                {
                    result = headings[length - 1];
                }
                else
                {
                    result = headings[i - 1];
                }

                break;
            }
        }

        return result;
    }

    public static Heading next(Heading current)
    {
        Heading result = current;
        final Heading[] headings = values();
        final int length = headings.length;
        for (int i = 0; i < length; i++)
        {
            Heading heading = headings[i];
            if (heading == current)
            {
                if (i == length - 1)
                {
                    result = headings[0];
                }
                else
                {
                    result = headings[i + 1];
                }

                break;
            }
        }

        return result;
    }
}
