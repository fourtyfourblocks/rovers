/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.model;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : Rover
 * <p/>
 * description :
 */
public class Rover
{
    public int x;
    public int y;
    public Heading heading;

    public Rover(int x, int y, Heading heading)
    {
        this.x = x;
        this.y = y;
        this.heading = heading;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("Rover{");
        sb.append("heading=").append(heading);
        sb.append(", x=").append(x);
        sb.append(", y=").append(y);
        sb.append('}');
        return sb.toString();
    }
}
    
