/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.model;

import com.fourtyfourblocks.rx.Observable;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : Command
 * <p/>
 * description :
 */
public class Command
{
    private final int x;
    private final int y;
    private final Heading heading;
    private final Observable<Move> commands;

    public Command(int x, int y, Heading heading, Observable<Move> commands)
    {
        this.x = x;
        this.y = y;
        this.heading = heading;
        this.commands = commands;
    }

    public Observable<Move> getCommands()
    {
        return commands;
    }

    public Heading getHeading()
    {
        return heading;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("Command{");
        sb.append(", x=").append(x);
        sb.append(", y=").append(y);
        sb.append(", heading=").append(heading);
        sb.append('}');
        return sb.toString();
    }
}
    
