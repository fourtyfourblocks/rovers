/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.model;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : Move
 * <p/>
 * description :
 */
public enum Move
{
    M, L, R
}
