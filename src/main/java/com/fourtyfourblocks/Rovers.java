package com.fourtyfourblocks;

import com.fourtyfourblocks.controller.RoverController;
import com.fourtyfourblocks.model.Rover;
import com.fourtyfourblocks.parser.CommunicationLink;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class Rovers
{
    private final static Logger logger = LoggerFactory.getLogger(Rovers.class);

    public static void main(String[] args)
    {
        logger.info("Rovers connecting ....");
        sendBack("Rovers connecting ....");

        final String inputFilePath = args.length > 0 ? args[0] : null;
        logger.debug("Start using input file {}.", inputFilePath);

        final Path path = getPath(inputFilePath);

        try (InputStream in = Files.newInputStream(path);
             BufferedReader reader = new BufferedReader(new InputStreamReader(in)))
        {
            final CommunicationLink communicationLink = new CommunicationLink(reader);
            sendBack("Connection established ...");
            final RoverController controller = communicationLink.initialize();
            Iterable<Rover> rovers = controller.navigate(communicationLink);
            for (Rover rover : rovers)
            {
                logger.info("moved rover to {}", rover);
                sendBack(String.format("%d %d %s", rover.x, rover.y, rover.heading.name()));
            }
        }
        catch (Exception e)
        {
            logger.error("Uh, houston we have a problem", e);
            sendBack("Uh, houston we have a problem, we cannot read from input file.");
            sendBack("Mission aborted.");
        }
    }

    private static Path getPath(String inputFilePath)
    {
        Path path;
        if (Strings.isNullOrEmpty(inputFilePath))
        {
            path = getDefaultPath(inputFilePath);
        }
        else
        {
            path = getInputPath(inputFilePath);
        }
        return path;
    }

    private static Path getInputPath(String inputFilePath)
    {
        Path path;
        try
        {
            path = Paths.get(inputFilePath);
            if (!Files.isReadable(path))
            {
                path = getDefaultPath(inputFilePath);
            }
        }
        catch (Exception e)
        {
            logger.error("There were problems constructing path to input file", e);
            path = getDefaultPath(inputFilePath);
        }
        return path;
    }

    private static Path getDefaultPath(String inputFilePath)
    {
        Path path;
        logger.warn("Could not read from file {}, fallback to default test input file.", inputFilePath);
        sendBack("Uh, there is no input file specified or cannot be read, using default resources/rover.in file.");
        sendBack("Usage: java -jar Rovers.jar ~/myRovers.in");
        try
        {
            final URI uri = Rovers.class.getResource("/rovers.in").toURI();
            logger.debug("Use this uri {} for default input file.", uri);
            if (uri.toString().contains("!"))
            {
                final String[] array = uri.toString().split("!");
                final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), new HashMap<String, String>());
                path = fs.getPath(array[1]);
            }
            else
            {
                path = Paths.get(uri);
            }
        }
        catch (Exception e)
        {
            logger.error("Uh, houston we have a problem.", e);
            sendBack("Uh, houston we have a problem, we cannot read input file.");
            throw new IllegalStateException("Cannot read any input file.", e);
        }
        return path;
    }

    private static void sendBack(String message)
    {
        System.out.println(message);
    }
}
