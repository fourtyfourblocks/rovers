/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.parser;

import com.fourtyfourblocks.controller.RoverController;
import com.fourtyfourblocks.model.Command;
import com.fourtyfourblocks.model.Heading;
import com.fourtyfourblocks.rx.Handler;
import com.fourtyfourblocks.rx.Observable;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

/**
 * date        : 05.05.14
 * author      : pawel
 * file name   : CommunicationLink
 * <p/>
 * description :
 */
public class CommunicationLink implements Observable<Command>
{
    private final static Logger logger = LoggerFactory.getLogger(CommunicationLink.class);

    private final BufferedReader reader;
    private final Pattern PLATEAU_PATTERN;
    private final Pattern INITIAL_POSITION_PATTERN;
    private final Pattern COMMANDS_PATTERN;

    public CommunicationLink(BufferedReader reader)
    {
        this.reader = reader;
        PLATEAU_PATTERN = Pattern.compile("(\\d)\\s(\\d)");
        INITIAL_POSITION_PATTERN = Pattern.compile("(\\d)\\s(\\d)\\s([NESW])");
        COMMANDS_PATTERN = Pattern.compile("^[MLR]*$");
    }

    public RoverController initialize() throws IOException
    {
        logger.info("Initializing rower's controller.");
        String line = reader.readLine();
        if (Strings.isNullOrEmpty(line))
        {
            throw new IllegalStateException("Cannot initialize plateau.");
        }

        final Matcher matcher = PLATEAU_PATTERN.matcher(line);
        if (matcher.find())
        {
            return new RoverController(parseInt(matcher.group(1)),
                                       parseInt(matcher.group(2)));
        }
        else
        {
            throw new IllegalStateException("Cannot initialize plateau.");
        }
    }

    @Override
    public void subscribe(Handler<Command> handler)
    {
        try
        {
            String line;
            Command command;
            while ((line = reader.readLine()) != null)
            {
                Matcher matcher = INITIAL_POSITION_PATTERN.matcher(line);
                if (matcher.find())
                {
                    int x = parseInt(matcher.group(1));
                    int y = parseInt(matcher.group(2));
                    Heading heading = Heading.valueOf(matcher.group(3));

                    line = reader.readLine();
                    if (line == null)
                    {
                        fail(handler, line);
                        break;
                    }
                    else
                    {
                        matcher = COMMANDS_PATTERN.matcher(line);
                        if (!matcher.find())
                        {
                            fail(handler, line);
                            break;
                        }
                    }

                    command = new Command(x, y, heading, new MoveObservable(line));
                    handler.next(command);
                }
                else
                {
                    fail(handler, line);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            handler.error(e);
        }

        handler.complete();
    }

    private void fail(Handler<Command> handler, String line)
    {
        logger.error("Input format is incorrect for {}.", line);
        handler.error(new IllegalArgumentException("Input format is incorrect, cannot recognize commands from " + line + "."));
    }
}
    
