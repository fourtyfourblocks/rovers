/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.parser;

import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.rx.Handler;
import com.fourtyfourblocks.rx.Observable;
import com.google.common.annotations.VisibleForTesting;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : MoveObservable
 * <p/>
 * description :
 */
public class MoveObservable implements Observable<Move>
{
    private final String line;

    public MoveObservable(String line)
    {
        this.line = line;
    }

    @Override
    public void subscribe(Handler<Move> handler)
    {
        try
        {
            for (int i = 0; i < line.length(); i++)
            {
                String l = String.valueOf(line.charAt(i));
                final Move move = Move.valueOf(l);

                handler.next(move);
            }
        }
        catch (IllegalArgumentException e)
        {
            handler.error(e);
        }

        handler.complete();
    }

    @VisibleForTesting
    public String getLine()
    {
        return line;
    }
}
    
