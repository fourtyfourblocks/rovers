/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.parser;

import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.rx.Handler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : MoveObservableTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class MoveObservableTest
{
    @Mock
    private Handler<Move> handler;

    private MoveObservable observable;

    @Before
    public void setUp() throws Exception
    {
        observable = new MoveObservable("MLRLRMMR");
    }

    @Test
    public void generateMovesStream() throws Exception
    {
        observable.subscribe(handler);

        verify(handler, times(3)).next(Move.M);
        verify(handler, times(2)).next(Move.L);
        verify(handler, times(3)).next(Move.R);

        verify(handler).complete();
        verify(handler, never()).error(any(Exception.class));
    }

    @Test
    public void failGeneratingMoves() throws Exception
    {
        // given N (unknown) as second move
        observable = new MoveObservable("MNRLRMMR");

        observable.subscribe(handler);

        verify(handler, times(1)).next(Move.M);
        verify(handler, never()).next(Move.L);
        verify(handler, never()).next(Move.R);

        verify(handler).complete();
        verify(handler).error(notNull(IllegalArgumentException.class));
    }
}
    
