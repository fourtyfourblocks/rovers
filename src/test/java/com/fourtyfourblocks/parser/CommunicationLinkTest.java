/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.parser;

import com.fourtyfourblocks.controller.RoverController;
import com.fourtyfourblocks.model.Command;
import com.fourtyfourblocks.model.Heading;
import com.fourtyfourblocks.rx.Handler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.*;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : CommunicationLinkTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class CommunicationLinkTest
{
    @Mock
    private BufferedReader reader;

    @Mock
    private Handler<Command> handler;

    private CommunicationLink link;

    @Before
    public void setUp() throws Exception
    {
        link = new CommunicationLink(reader);
    }

    @Test
    public void initialize_noPlateauDimensions() throws Exception
    {
        assertFailForWrongDimensions(null);
        assertFailForWrongDimensions("");
        assertFailForWrongDimensions("1");
        assertFailForWrongDimensions("1 ");
        assertFailForWrongDimensions("1 A");
        assertFailForWrongDimensions("A 1");
        assertFailForWrongDimensions("11");
    }

    @Test
    public void initialize() throws Exception
    {
        // given
        doReturn("4 3").when(reader).readLine();

        // when
        final RoverController controller = link.initialize();

        // then
        assertThat(controller.getMaxX()).isEqualTo(4);
        assertThat(controller.getMaxY()).isEqualTo(3);
    }

    @Test
    public void generateCommandsStreamOnSubscribe() throws Exception
    {
        doReturn("1 2 N")
                .doReturn("LMLMLMLMM")
                .doReturn("3 3 E")
                .doReturn("MMRMMRMRRM")
                .doReturn(null)
                .when(reader).readLine();

        final ArgumentCaptor<Command> captor = ArgumentCaptor.forClass(Command.class);

        // when
        link.subscribe(handler);

        // then
        verify(handler, times(2)).next(captor.capture());
        verify(handler).complete();
        verify(handler, never()).error(any(Exception.class));

        final List<Command> commands = captor.getAllValues();
        assertCommand(commands.get(0), 1, 2, Heading.N, "LMLMLMLMM");
        assertCommand(commands.get(1), 3, 3, Heading.E, "MMRMMRMRRM");
    }

    @Test
    public void failGeneratingCommandsOnInvalidInput() throws Exception
    {
        doReturn("1 2 N")
                .doReturn("LMLMLMLMM")
                .doReturn("3 3 E")
                .doReturn("3 4 E")
                .doReturn("MMRMMRMRRM")
                .doReturn(null)
                .when(reader).readLine();

        // when
        link.subscribe(handler);

        // then
        verify(handler).next(notNull(Command.class));
        verify(handler).error(notNull(IllegalArgumentException.class));
        verify(handler).complete();
    }

    @Test
    public void failGeneratingCommandsOnNullCommands() throws Exception
    {
        doReturn("1 2 N")
                .doReturn(null)
                .when(reader).readLine();

        link.subscribe(handler);

        verify(handler, never()).next(notNull(Command.class));
        verify(handler).error(notNull(IllegalArgumentException.class));
        verify(handler).complete();
    }

    @Test
    public void failOnReadingPosition() throws Exception
    {
        doReturn("1 2 A")
                .doReturn("LMLMLMLMM")
                .when(reader).readLine();

        link.subscribe(handler);

        verify(handler, never()).next(any(Command.class));
        verify(handler).error(notNull(IllegalArgumentException.class));
        verify(handler).complete();
    }

    @Test
    public void failOnUnexpectedException() throws Exception
    {
        doThrow(new IOException("test io exception"))
                .when(reader).readLine();

        link.subscribe(handler);

        verify(handler, never()).next(any(Command.class));
        verify(handler).error(notNull(IllegalArgumentException.class));
        verify(handler).complete();
    }

    private void assertCommand(Command command, int expectedX, int expectedY, Heading expectedHeading, String commands)
    {
        assertThat(command.getX()).isEqualTo(expectedX);
        assertThat(command.getY()).isEqualTo(expectedY);
        assertThat(command.getHeading()).isEqualTo(expectedHeading);
        assertThat(((MoveObservable) command.getCommands()).getLine()).isEqualTo(commands);
    }

    private void assertFailForWrongDimensions(String dimensions) throws IOException
    {
        try
        {
            doReturn(dimensions).when(reader).readLine();
            link.initialize();
            throw fail("Should fail with IllegalStateException");
        }
        catch (IllegalStateException e)
        {
            //ok
        }
        reset(reader);
    }
}
    
