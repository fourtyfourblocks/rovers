/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.fest.assertions.Assertions.assertThat;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : RoversTest
 * <p/>
 * description : No assertions
 */
public class RoversTest
{
    private PrintStream oldSout;
    private ByteArrayOutputStream baos;

    @Before
    public void setUp() throws Exception
    {
        baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        // IMPORTANT: Save the old System.out!
        oldSout = System.out;
        // Tell Java to use your special stream
        System.setOut(ps);
    }

    @After
    public void tearDown() throws Exception
    {
        System.setOut(oldSout);
    }

    @Test
    public void useDefaultInputFile() throws Exception
    {
        Rovers.main(new String[0]);

        soutContains("1 3 N");
        soutContains("5 1 E");
    }

    @Test
    public void useTestInputFile() throws Exception
    {
        final String path = getClass().getResource("/test-rovers.in").toString().replace("file:/", "");
        Rovers.main(new String[]{path});

        soutContains("1 3 N");
        soutContains("Input format is incorrect for 3 3 E");
    }

    private void soutContains(String expected)
    {
        System.out.flush();
        assertThat(baos.toString()).contains(expected);
    }
}
    
