/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.controller;

import com.fourtyfourblocks.model.Command;
import com.fourtyfourblocks.model.Heading;
import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.model.Rover;
import com.fourtyfourblocks.rx.Handler;
import com.fourtyfourblocks.rx.Observable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.verify;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : RoverControllerTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class RoverControllerTest
{
    @Mock
    private Observable<Command> observable;

    @Mock
    private Observable<Move> moveObservable;

    private RoverController controller;
    private RoverNavigator navigator;
    private Rover rover;

    @Before
    public void setUp() throws Exception
    {
        controller = new RoverController(3, 5);
        navigator = controller;

        rover = new Rover(2, 3, Heading.E);
    }

    /*
     * NOTE this is a bit tricky
     *
     * 1. register for command stream and get handler
     * 2. mock interaction by sending commands
     * 3. Iterable of Rovers should be filled up after interaction as it is same ref
     */
    @Test
    @SuppressWarnings("unchecked")
    public void navigate() throws Exception
    {
        final ArgumentCaptor<Handler> captor = ArgumentCaptor.forClass(Handler.class);

        // when
        final Iterable<Rover> rovers = controller.navigate(observable);
        assertThat(rovers).hasSize(0);

        // and if
        verify(observable).subscribe(captor.capture());
        final Handler<Command> handler = captor.getValue();
        handler.next(new Command(2, 3, Heading.E, moveObservable));

        // then
        assertThat(rovers).hasSize(1);
        final Rover next = rovers.iterator().next();
        assertThat(next.x).isEqualTo(2);
        assertThat(next.y).isEqualTo(3);
        assertThat(next.heading).isEqualTo(Heading.E);
        verify(moveObservable).subscribe(notNull(Handler.class));
    }

    @Test
    public void canMove() throws Exception
    {
        // in the middle
        assertThat(navigator.canMove(new Rover(1, 3, Heading.S), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(1, 3, Heading.W), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(1, 3, Heading.E), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(1, 3, Heading.N), Move.M)).isTrue();

        // vertical edges
        assertThat(navigator.canMove(new Rover(1, 4, Heading.N), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(1, 5, Heading.N), Move.M)).isFalse();

        assertThat(navigator.canMove(new Rover(1, 1, Heading.S), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(1, 0, Heading.S), Move.M)).isFalse();

        // horizontal edges
        assertThat(navigator.canMove(new Rover(1, 4, Heading.W), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(0, 5, Heading.W), Move.M)).isFalse();

        assertThat(navigator.canMove(new Rover(2, 1, Heading.E), Move.M)).isTrue();
        assertThat(navigator.canMove(new Rover(3, 0, Heading.E), Move.M)).isFalse();
    }

    @Test
    public void move() throws Exception
    {
        // given rover at position 2, 3 E
        rover = navigator.move(rover, Move.M);
        assertThat(rover.x).isEqualTo(3);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(Heading.E);

        rover.heading = Heading.W;
        rover = navigator.move(rover, Move.M);
        assertThat(rover.x).isEqualTo(2);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(Heading.W);

        rover.heading = Heading.N;
        rover = navigator.move(rover, Move.M);
        assertThat(rover.x).isEqualTo(2);
        assertThat(rover.y).isEqualTo(4);
        assertThat(rover.heading).isEqualTo(Heading.N);

        rover.heading = Heading.S;
        rover = navigator.move(rover, Move.M);
        assertThat(rover.x).isEqualTo(2);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(Heading.S);
    }

    @Test
    public void skipMove() throws Exception
    {
        final Rover rover = navigator.move(new Rover(3, 3, Heading.E), Move.M);
        assertThat(rover.x).isEqualTo(3);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(Heading.E);
    }

    @Test
    public void ignoreInvalidMove() throws Exception
    {
        final Rover rover = navigator.move(new Rover(2, 3, Heading.E), Move.L);
        assertThat(rover.x).isEqualTo(2);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(Heading.E);
    }

    @Test
    public void rotateLeft() throws Exception
    {
        // given rover at position 2, 3 E
        assertRotate(Move.L, Heading.N);
        assertRotate(Move.L, Heading.W);
        assertRotate(Move.L, Heading.S);
        assertRotate(Move.L, Heading.E);
    }

    @Test
    public void rotateRight() throws Exception
    {
        // given rover at position 2, 3 E
        assertRotate(Move.R, Heading.S);
        assertRotate(Move.R, Heading.W);
        assertRotate(Move.R, Heading.N);
        assertRotate(Move.R, Heading.E);
    }

    @Test
    public void ignoreInvaliRotate() throws Exception
    {
        // given rover at position 2, 3 E
        assertRotate(Move.M, Heading.E);
    }

    private void assertRotate(Move leftMove, Heading expectedHeading)
    {
        rover = navigator.rotate(rover, leftMove);
        assertThat(rover.x).isEqualTo(2);
        assertThat(rover.y).isEqualTo(3);
        assertThat(rover.heading).isEqualTo(expectedHeading);
    }
}
    
