/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.controller;

import com.fourtyfourblocks.model.Heading;
import com.fourtyfourblocks.model.Move;
import com.fourtyfourblocks.model.Rover;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * date        : 06.05.14
 * author      : pawel
 * file name   : MoveHandlerTest
 * <p/>
 * description :
 */
@RunWith(MockitoJUnitRunner.class)
public class MoveHandlerTest
{
    @Mock
    private RoverNavigator navigator;

    private Rover rover;
    private MoveHandler handler;

    @Before
    public void setUp() throws Exception
    {
        rover = new Rover(1, 3, Heading.W);
        handler = new MoveHandler(rover, navigator);
    }

    @Test
    public void next() throws Exception
    {
        handler.next(Move.M);
        handler.next(Move.L);
        handler.next(Move.R);

        verify(navigator).move(rover, Move.M);
        verify(navigator).rotate(rover, Move.R);
        verify(navigator).rotate(rover, Move.L);
    }
}
    
